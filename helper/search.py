"""
    Created by 范钟<coder.zf@outlook.com> on 2019/04/06
"""


def is_isbn(work):
    """
    判断是否是isbn
    isbn isbn13 13个0-9的数字组成的
    isbn10 10个0-9数字组成，含有一些-
    :param work:
    :return:
    """
    if len(work) == 13 and work.isdigit():
        return True

    short_work = work.replace('-', '')
    if '-' in work and len(short_work) == 10 and short_work.isdigit():
        return True
    return False
