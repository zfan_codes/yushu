"""
    Created by 范钟<coder.zf@outlook.com> on 2019/04/06
"""

import requests


class HTTP:

    @staticmethod
    def get(url, result_json=True):
        r = requests.get(url)
        if r.status_code != 200:
            return {} if result_json else ""
        return r.json() if result_json else r.text

    @staticmethod
    def post(url, data, result_json=True):
        r = requests.post(url, data=data)
        if result_json:
            return r.json()
        return r.text
