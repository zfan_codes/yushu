"""
    Created by 范钟<coder.zf@outlook.com> on 2019/04/06
"""
from sqlalchemy import Column, Integer, String
from models.base import Base


class Book(Base):
    id = Column(Integer, primary_key=True, autoincrement=True)
    title = Column(String(50), nullable=False)
    aurhor = Column(String(30), default="佚名")
    binding = Column(String(32))  # 平装
    publisher = Column(String(64))  # 出版社
    price = Column(String(64))  # 价格
    pages = Column(Integer)  # 页数
    pubdate = Column(String(32))  # 出版时间
    isbn = Column(String(32), nullable=False, unique=True)
    summary = Column(String(1000))  # 简介
    image = Column(String(200))  # 图片
