"""
    Created by 范钟<coder.zf@outlook.com> on 2019/04/07
"""
from sqlalchemy import Column, Integer, Boolean, ForeignKey, String
from sqlalchemy.orm import relationships

from models.base import Base


class Gift(Base):
    id = Column(Integer, primary_key=True)
    user = relationships('User')  # 外键名称
    uid = Column(Integer, ForeignKey('user.id'))  # 指定外键ID
    # book = relationships('Book')
    # bid = Column(Integer,ForeignKey('book.isbn'))
    isbn = Column(String(64), nullable=False)
    launched = Column(Boolean, default=False)
