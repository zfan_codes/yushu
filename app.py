"""
    Created by 范钟<coder.zf@outlook.com> on 2019/04/06
"""

from app import create_app

app = create_app()

if __name__ == '__main__':
    app.run(debug=True)
