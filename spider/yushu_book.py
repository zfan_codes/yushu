"""
    Created by 范钟<coder.zf@outlook.com> on 2019/04/06
"""
from helper.http import HTTP
from flask import current_app


class YuShuBook:
    isbn_url = "http://t.yushu.im/v2/book/isbn/{}"
    keyword_url = "http://t.yushu.im/v2/book/search?q={}&count={}&start={}"

    def __init__(self):
        self.total = 0
        self.books = []

    def search_by_isbn(self, isbn):
        url = self.isbn_url.format(isbn)
        data = HTTP.get(url)
        self.__fill_single(data)

    def search_by_keyword(self, keyword, page=1):
        per_page = current_app.config['PER_PAGE']
        url = self.keyword_url.format(keyword, per_page, self.calculate_start(page))
        data = HTTP.get(url)
        self.__fill_collection(data)

    def __fill_single(self, data):
        if data:
            self.total = 1
            self.books.append(data)

    def __fill_collection(self, data):
        self.total = data['total']
        self.books = data['books']

    def calculate_start(self, page):
        per_page = current_app.config['PER_PAGE']
        return (page - 1) * per_page

    @property
    def first(self):
        return self.books[0] if self.total >= 1 else None
