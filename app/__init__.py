"""
    Created by 范钟<coder.zf@outlook.com> on 2019/04/06
"""
from flask import Flask
from flask_login import LoginManager

from models.base import db

login_manager = LoginManager()


def create_app():
    app = Flask(__name__, static_folder="../static")

    app.config.from_object('app.setting')
    # 导入机密信息，覆盖设置
    config_secure(app)
    register_blueprint(app)

    # 注册用户登陆模块
    login_manager.init_app(app)
    login_manager.login_view = 'web.login'
    login_manager.login_message = "请先登陆或注册"

    # 注册数据库
    db.init_app(app)
    with app.app_context():
        db.create_all()
    return app


def register_blueprint(app):
    from app.web import web
    app.register_blueprint(web)


def config_secure(app):
    try:
        app.config.from_object('app.secure')
    except Exception:
        pass
