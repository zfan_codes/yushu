"""
    Created by 范钟<coder.zf@outlook.com> on 2019/04/07
"""
from flask_login import login_required

from . import web


@web.route("/my/gifts")
@login_required
def my_gifts():
    return 'My gifts'


@web.route('/my_wish/')
def my_wish():
    pass


@web.route('/pending/')
def pending():
    pass
