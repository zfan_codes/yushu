"""
    Created by 范钟<coder.zf@outlook.com> on 2019/04/06
"""
from flask import jsonify, current_app, render_template, flash

from . import web


@web.route("/test/config/")
def config():
    return jsonify(current_app.config['PER_PAGE'])


@web.route("/test/html")
def test():
    r = {
        'name': "张三",
        'age': 18
    }
    return render_template("test/test.html", data=r)


@web.route("/test/html2")
def test2():
    r = [
        "张三",
        "里斯",
        "王五"
    ]
    maps = {
        "name": "张三",
        "age": 18,
        "sex": "男"
    }
    flash("hello world!", 'error')
    return render_template("test/test2.html", data=r, maps=maps)
