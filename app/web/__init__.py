"""
    Created by 范钟<coder.zf@outlook.com> on 2019/04/06
"""
from flask import Blueprint

web = Blueprint('web', __name__, template_folder="templates")

from . import book, test, gift, auth, main
