"""
    Created by 范钟<coder.zf@outlook.com> on 2019/04/06
"""
DEBUG = True  # 调试模式
PER_PAGE = 15  # 分页显示数量
SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:123456@localhost:3306/yushu?charset=utf8'
SQLALCHEMY_TRACK_MODIFICATIONS = True
SECRET_KEY = '9d0887eb-c5a3-46a4-9c7c-847aaa4512ce'
