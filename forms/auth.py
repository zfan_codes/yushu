"""
    Created by 范钟<coder.zf@outlook.com> on 2019/04/08
"""
from wtforms import Form, StringField, PasswordField
from wtforms.validators import DataRequired, Length, Email, ValidationError

from models.user import User


class RegisterForm(Form):
    email = StringField(
        validators=[
            DataRequired(message="邮件不能为空"),
            Email(message="电子邮箱不符合规范")
        ]
    )
    password = PasswordField(
        validators=[
            DataRequired(message='密码不能为空'),
            Length(6, 32)
        ]
    )

    nickname = StringField(
        validators=[
            DataRequired(message="用户昵称不能为空"),
            Length(2, 10, message="用户昵称至少需要两个字符，不能超过10个字符")
        ]
    )

    # 自定义验证其
    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError("电子邮件已经被注册")

    def validate_nickname(self, field):
        if User.query.filter_by(nickname=field.data).first():
            raise ValidationError("昵称已经被注册")


class LoginForm(Form):
    email = StringField(
        validators=[
            Email(message="邮箱格式不符合要求"),
            DataRequired(message="请输入邮箱格式")
        ]
    )

    password = PasswordField(
        validators=[
            DataRequired(message="请输入密码")
        ]
    )
