"""
    Created by 范钟<coder.zf@outlook.com> on 2019/04/06
"""
from wtforms import Form, StringField, IntegerField
from wtforms.validators import Length, NumberRange, DataRequired


class SearchForm(Form):
    q = StringField(validators=[Length(min=1, max=32), DataRequired()])
    page = IntegerField(validators=[NumberRange(min=1, max=99)], default=1)
